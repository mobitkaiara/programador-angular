import { InjectionToken } from '@angular/core';

export const APP_CONFIG = new InjectionToken<any>('app.config');

export const API_REST_ENDPOINT: any = "http://localhost:8080/programmer-java/api";