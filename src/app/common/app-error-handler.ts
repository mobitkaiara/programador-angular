import { Injectable, ErrorHandler } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { logErro } from 'src/environments/environment';

@Injectable()
export class AppErrorHandler implements ErrorHandler {
  handleError(error: any) {
    if (error instanceof HttpErrorResponse) {
      //Backend returns unsuccessful response codes such as 404, 500 etc.
      if (error.status == 401) {
        if (logErro) {
          console.log(error)
        }
      }

      if (logErro) {
        console.error('Backend retornou o código de status: ', error.status);
        console.error('Response body:', error.message);
      }
    } else if (error.error instanceof ErrorEvent) {
      if (logErro) {
        console.error('Um erro:', error.message);
      }
    } else {
      //A client-side or network error occurred.	          
      if (logErro) {
        console.error('Um erro ocorreu:', error.message);
      }
    }
  }
} 