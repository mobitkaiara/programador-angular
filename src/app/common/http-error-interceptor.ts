import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NOT_FOUND, UNAUTHORIZED, BAD_REQUEST, FORBIDDEN, getStatusText } from 'http-status-codes';
import { logErro } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class HttpErrorInterceptor implements HttpInterceptor {
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                let errMsg = '';
                // Client Side Error
                if (error.error instanceof ErrorEvent) {
                    errMsg = `Error: ${error.error.message}`;
                } else {
                    // Server Side Error
                    if (logErro) {
                        return Observable.throw(error.message);
                    }

                    const statusCode = error.status;

                    switch (statusCode) {
                        case NOT_FOUND:
                            return Observable.throw(error.message);
                            break;
                        case BAD_REQUEST:
                            return Observable.throw(error.message);
                            break;
                        case UNAUTHORIZED:
                            return Observable.throw(error.message);
                            break;
                        case FORBIDDEN:
                            return Observable.throw(error.message);
                            break;
                    }

                    errMsg = getStatusText(statusCode);
                }

                if (logErro) {
                    return Observable.throw(error.message);
                }

                return throwError(error);
            })
        );
    }
}