import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { tap } from "rxjs/operators";
import { Router } from "@angular/router";

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
      return next.handle(req)
        .pipe(
          tap(
            event => {},
            error => {
              if (error.status === 401) {
                console.log("Erro 401");
              }
            }
          )
        );
    }
}
