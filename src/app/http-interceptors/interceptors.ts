import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor() { }
    intercept(req: HttpRequest<any>, next: HttpHandler) {

        !req.headers.has("Content-Type") && (req = req.clone({ headers: req.headers.set("Content-Type", "application/json") }));

        return next.handle(req);
    }
}
