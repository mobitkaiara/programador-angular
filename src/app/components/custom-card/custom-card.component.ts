import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-custom-card",
  inputs: ["header"],
  templateUrl: "./custom-card.component.html",
  styleUrls: ["./custom-card.component.sass"]
})
export class CustomCardComponent implements OnInit {
  header: string;

  constructor() { }

  ngOnInit() { }
}
