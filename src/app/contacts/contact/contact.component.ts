import { Component, OnInit } from '@angular/core';
import { ContactsService } from 'src/app/services/contacts.service';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {
  public contactId: any;

  public contactForm: FormGroup = this.formBuilder.group({
    id: [""],
    name: ["", Validators.required],
    email: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    ])), 
    address: [""],
    phones: ["", Validators.required],
  });

  constructor(
    private contactService: ContactsService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.contactId = this.route.snapshot.params.contactId;

    if (this.contactId) {
      this.getContactById();
    }
  }

  getContactById() {
    this.contactService.getContactById(this.contactId).subscribe((data: any) => {
      this.contactForm.patchValue({ ...data });
    });
  }

  save() {
    this.contactService.saveContact(this.contactForm.value).subscribe(
      resp => {
        this.toastr.success("Dados salvos com sucesso!");
        this.router.navigate(["/home"]);
      },
      err => {
        err => this.toastr.error("Erro ao salvar dados!");
      }
    );
  }

  update() {
    this.contactService.updateContact(this.contactForm.value).subscribe(
      resp => {
        this.toastr.success("Dados salvos com sucesso!");
      },
      err => {
        err => this.toastr.error("Erro ao salvar dados!");
      }
    );
  }
}