import { Component, OnInit, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { ContactsService } from '../services/contacts.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.sass']
})
export class ContactsComponent implements OnInit {
  public contacts: any;
  public contactCodSearch;

  constructor(
    private contactService: ContactsService,
    private renderer: Renderer2,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getContacts();
  }

  @ViewChild("noResult", { static: true }) noResult: ElementRef;

  getContacts() {
    this.contactService.getContacts().subscribe((data: any) => {
      this.contacts = data;
    });
  }

  deleteContact(idContact: any) {
    this.contactService.deleteContact(idContact).subscribe(
      resp => {
        this.getContacts();
        this.toastr.success("Contato removido com sucesso!");
      },
      err => {
        err => this.toastr.error("Erro ao remover contato!");
      }
    );
  }

  noResultFn() {
    const renderer = this.renderer;
    const element = this.noResult.nativeElement;

    return (value: boolean) => {
      value
        ? renderer.removeAttribute(element, "hidden")
        : renderer.setAttribute(element, "hidden", "");
    };
  }
}
