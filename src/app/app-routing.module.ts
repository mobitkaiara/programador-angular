import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidebarLayoutComponent } from './layout/sidebar-layout/sidebar-layout.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactComponent } from './contacts/contact/contact.component';

const routes: Routes = [
  {
    path: "",
    component: SidebarLayoutComponent,
    children: [
      { path: "", redirectTo: 'home', pathMatch: 'prefix' },
      { path: "home", component: ContactsComponent },
      { path: "contact", component: ContactComponent },
      { path: "contact/:contactId", component: ContactComponent }
    ]
  },
  {
    path: "**",
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: "reload" })],
  exports: [RouterModule],
})
export class AppRoutingModule { }

export const routingComponents = [
  SidebarLayoutComponent
];
