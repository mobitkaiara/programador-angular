import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-sidebar-layout",
  templateUrl: "./sidebar-layout.component.html",
  styleUrls: ["./sidebar-layout.component.sass"]
})
export class SidebarLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }

}
