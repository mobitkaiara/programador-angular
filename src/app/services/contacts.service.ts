import { Injectable } from '@angular/core';
import { apiUrl } from "src/environments/environment";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(private http: HttpClient) { }

  getContacts() {
    return this.http.get(`${apiUrl}/contact/`);
  }

  getContactById(idContact) {
    return this.http.get(`${apiUrl}/contact/${idContact}`);
  }

  saveContact(data: any) {
    return this.http.post<any>(`${apiUrl}/contact/`, data);
  }

  updateContact(data: any) {
    return this.http.put<any>(`${apiUrl}/contact/`, data);
  }

  deleteContact(idContact) {
    return this.http.delete(`${apiUrl}/contact/${idContact}`);
  }
}
