import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from './app.component';
import { AppRoutingModule, routingComponents } from "./app-routing.module";
import { CustomCardComponent } from './components/custom-card/custom-card.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactComponent } from './contacts/contact/contact.component';
import { SidebarLayoutComponent } from './layout/sidebar-layout/sidebar-layout.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ToastrModule } from 'ngx-toastr';
import { FilterContactPipe } from './pipes/filter-contact.pipe';
import { httpInterceptorProviders } from './http-interceptors';
import { API_REST_ENDPOINT } from "./app.config";
@NgModule({
  declarations: [
    AppComponent,
    CustomCardComponent,
    ContactsComponent,
    ContactComponent,
    routingComponents,
    SidebarLayoutComponent,
    FilterContactPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    httpInterceptorProviders,
    { provide: "API", useValue: API_REST_ENDPOINT }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
