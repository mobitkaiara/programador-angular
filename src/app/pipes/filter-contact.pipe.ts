import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterContact'
})
export class FilterContactPipe implements PipeTransform {
  transform(
    items: Array<any>,
    nameSearch: string,
    noResultFn
  ) {
    if (items && items.length) {
      let results = items.filter(item => {
        if (nameSearch && item.name.trim().toLowerCase().indexOf(nameSearch.trim().toLowerCase()) === -1) {
          return false;
        }

        return true;
      });

      noResultFn(!results.length);

      return results;
    } else {
      noResultFn(false);
      return items;
    }
  }
}
