## Mobit

O projeto foi constrúido utilizando o Angular 9.

Para a execução do projeto é preciso rodar os seguintes comandos:

**npm install** ou **yarn install**

Em seguida:

**ng serve**

O projeto ira executar na porta **4200**